# blockRQ_RCG 

MATLAB software that implements the ``blockRQ_RCG`` algorithm from the Arxiv preprint [Gradient-type subspace iteration methods for the symmetric eigenvalue problem](https://arxiv.org/abs/2306.10379).

## Installation and usage

Download [release 1.0](https://gitlab.unige.ch/Bart.Vandereycken/blockRQ_RCG/-/releases/v1.0) or clone the repository from one of the links on the top of this page. 

There is no particular installation needed in MATLAB since all files are included but remember to call ``startup.m`` to add directories to the path.

The file ``example.m`` is a simple example on how to use ``blockRQ_RCG.m``. 

For more advanced uses with large matrices, see the files in the directory ``experiments_paper``.

## Reproducing numerical experiments

All numerical experiments from the above preprint can be reproduced using the scripts in the directory ``experiments_paper``.

The figures and the tables in the preprint were obtained using version 1.0 of the software.

## Credits

The code is based on joint work by Foivos Alimis, Yousef Saad, and Bart Vandereycken.

This software is GPL licensed. When using it for academic purposes, please cite the following preprint:

```
@article{
    title = {Gradient-type subspace iteration methods for the symmetric eigenvalue problem},
    journal = {arXiv:2306.10379},
    author = {Alimisis, Foivos and Saad, Yousef and Vandereycken, Bart},
    doi = {10.48550/arXiv.2306.10379},
    year = {2024}, 
}
```
This work was partly supported by the Swiss National Science Foundation under research projects 192129 and 192363.

