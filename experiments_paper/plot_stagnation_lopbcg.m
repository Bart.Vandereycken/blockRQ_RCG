function plot_stagnation_lopbcg()
% Code to reproduce Figs 6.6

% First do run_many_matrices_lobpcg() to produce the computation stats.


files = dir('computations/LOBPCG_ACTIVSg70K-p32-RR50-max.mat');


for k=1:length(files)
    filename = files(k).name;

    S = load(sprintf('computations/%s',filename));
    prob_name = strrep(extractBefore(filename,".mat"), '_', '-');
    disp(sprintf('Loading %s', filename))

    methods = {'CG', 'CGm', 'LO', 'LOs'};
    legend_names = {'RCG', 'RCG(+)', 'LOBCG', 'LOBCG(+)'};
    S.stats


    err_angle = {};
    err_eval = {};

    if isfield(S,'Q')
        p = size(S.Q.CG,2);
        Qex = S.stats.eigs.evecs; Qex = Qex(:,1:p);
        dex = S.stats.eigs.evals_eigs; dex = dex(1:p);

        for m = methods
            meth = m{1};

            Q = S.Q.(meth);
            p = size(Q,2);
            err_angle.(meth) = subspace(Q,Qex);
            d = sort(eig(S.Dq.(meth)), 'descend');
            err_eval.(meth) = norm(d-dex, 'inf')/norm(dex, 'inf');
        end
        f_ex = sum(dex);


    end

    nice_cols = plot_defaults();

    close all
    figure(1);
    for i = 1:length(methods)
        meth = methods{i};
        t = S.times.(meth);
        r = S.res.(meth);
        l = min(length(t),length(r));
        semilogy(t(1:l), r(1:l), '-s','linewidth',2, 'color', nice_cols(i,:), 'DisplayName', legend_names{i})
        hold on
    end
    title(prob_name)
    axis([0 130 1e-8 1e5])
    legend()
    xlabel('Time (s.)')
    ylabel('Residual in infty norm')
    saveas(gcf,sprintf('figs_paper/stagnation_residual_%s',prob_name),'epsc')

end


return




