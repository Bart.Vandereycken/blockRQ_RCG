function run_many_matrices()
% Code to reproduce Figs 6.2-5

% Depending on the values chosen below, it will run
% many matrices for several options.
% It stores the results with stats so they can be summarized
% and displayed later using plot_many_matrices.m


a_nb_RR_steps = [25 50 100];
maxit = 2000; % large maxit to check for any unstable iterations
a_p = [32 64];
a_minmax = [+1 -1];
a_name = {'FD3D', 'ukerbe1', 'boneS01', 'ACTIVSg70K'};

for nb_RR_steps = a_nb_RR_steps
    for p = a_p
        for minmax = a_minmax
            for name = a_name
                name = name{1};
                if strcmp(name, 'FD3D')
                    A = fd3d(35,40,25,0,0,0,0);
                else
                    load(name)
                    A = Problem.A;
                end

                if minmax == 1
                    filename = sprintf('%s-p%i-RR%i-max', name, p, nb_RR_steps);
                else
                    filename = sprintf('%s-p%i-RR%i-min', name, p, nb_RR_steps);
                end
                filename
                do_its(A,p,nb_RR_steps,maxit, filename, minmax);
            end
        end
    end
end
end



function [Q, Dq, trc, res, its, stats] = do_its(A,m,nb_RR_steps,maxit,filename, minmax)
p = m;
% set seed
rng(123)

% m = size of iteration subspace, p = nb of e-vals wanted
% here m = p since no RR extraction
n = size(A,1);
% initial subspace
Q0 = randn(n,m);
A = minmax*A;

%%% Compute the optimal Chebyshev filter using the exact eigenvalues
% d = sort(eig(full(A)), 'descend');
t=tic();
[V1,D1,flag1] = eigs(A,m+6, 'largestreal',  'Display', 0);
if flag1
    warning('--- EIGS did not converge for largestreal')
end
if strcmp(filename(1:5), 'boneS') && minmax==1
    disp('--- since boneS is SDP, we take zero as min eval')
    D2 = 0;
    V2 = zeros([size(V1,1) 1]);
    flag2 = 0;
elseif strcmp(filename(1:6), 'audikw') && minmax==1
    disp('--- since audikw is SDP, we take zero as min eval')
    D2 = 0;
    V2 = zeros([size(V1,1) 1]);
    flag2 = 0;
else
    [V2,D2,flag2] = eigs(A,6,   'smallestreal', 'Display', 0);
    if flag2
        warning('--- EIGS did not converge for smallestreal')
    end
end

[d1,ind1] = sort(diag(D1), 'descend'); V1 = V1(:,ind1);
[d2,ind2] = sort(diag(D2), 'descend'); V2 = V2(:,ind2);
stats.time_eigs = toc(t);
d = [d1; d2];
V = [V1 V2];


sprintf('--- Target eigenvalues max,min %10.8f %10.8f\n',d(1),d(p))

lmin_unwanted = d(end);
lmax_unwanted = d(p+1);

% do iterations
% `its` stores nb of matvecs with A (or A-cI)

trc = struct(); res = struct(); its = struct();
if ~flag1 && ~flag2
    %% Standard SI on B = a(A-cI) which is optimal degree 1 poly for given unwanted spectrum
    [Q.SI0, Dq.SI0, trc.SI0, res.SI0, its.SI0] = subsit0(A, Q0, maxit, nb_RR_steps, lmax_unwanted, lmin_unwanted);

    %% Standard SI on B = p_d(A) with optimal degree d = nb_RR_steps poly for given unwanted spectrum
    [Q.SICheb, Dq.SICheb, trc.SICheb, res.SICheb, its.SICheb] = subsitCh(A, Q0, maxit, nb_RR_steps, lmax_unwanted, lmin_unwanted);
end

%% Riemanian SD / CG on A (applying the poly filter has no benefit)
opts.TOL_REL_RESIDUAL = 0;
opts.SAVE_EXTRA_MULT_A = true;
opts.EXTRA_ORTH_MOD_ITS = Inf;
opts.RESET_BETA_MOD_ITS = 75;
[Q.SD, Dq.SD,trc.SD, res.SD, its.SD] = blockRQ_RCG(A, Q0, maxit, nb_RR_steps, false, opts);
[Q.CG, Dq.CG,trc.CG, res.CG, its.CG] = blockRQ_RCG(A, Q0, maxit, nb_RR_steps, true, opts);

%% Stats
stats.delta = d(p)-d(p+1);
stats.cond_H = (d(1)-d(end))/(d(p)-d(p+1));
stats.eigs.evals_eigs = d;
stats.eigs.evecs = V;
stats.eigs.flag = [flag1 flag2];

save(sprintf('computations/full_%s.mat', filename), 'A','Q','Dq','trc','res','its','stats')
save(sprintf('computations/%s.mat', filename), 'trc','res','its','stats')
end
