function table_many_matrices_lopbcg()
% Code to reproduce Table 6.1.

% First do run_many_matrices_lobpcg() to produce the computation stats.

tols = [1e-8];
files = dir('computations/LOBPCG_*.mat');

for tol=tols
    fprintf("SHOWING TOL = %e \n", tol)
    for k=1:length(files)
        % start with a new line for each file
        line = "";
        filename = files(k).name;
        S = load(sprintf('computations/%s',filename));
        prob_name = strrep(extractBefore(filename,".mat"), '_', '-');
        split_name = split(prob_name, "-");
        table_name = sprintf("%s p=%s %s", split_name{2}, strrep(split_name{3}, "p",""), split_name{5});

        line = line + table_name + " & ";

        %disp(sprintf('Loading %s', filename));

        methods = {'LO', 'LOs', 'CG', 'CGm'};

        for i_m = 1:length(methods)
            meth = methods{i_m};
            res = S.res.(meth);
            times = S.times.(meth);
            its = S.its.(meth);

            it = find(res/res(1)<tol, 1, 'first');
            if isempty(it)
                line = line + sprintf("   *   &   *   ");
            else
                line = line + sprintf(" %4.0f & %4.0f ",times(it), its(it));
            end

            % we make a table where each line corresponds to a problem
            if i_m < length(methods)
                line = line + " & ";
            else
                line = line + " || \n";
            end
        end

        % done with this file, show the line
        fprintf(strrep(line,"|", "\\"))

    end
end

return




