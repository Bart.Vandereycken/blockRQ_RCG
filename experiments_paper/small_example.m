% Code to reproduce Fig 6.1.

clear all

start_close = false;

% set seed
rng(123)

% 2D fin diff Laplacian
A = fd3d(35,40,1,0,0,0,0); % 2D since nz = 1

% m = size of iteration subspace, p = nb of e-vals wanted
% here m = p since no RR extraction
m = 6; p = m;  
n = size(A,1);
n
maxit = 1000; % large maxit to check for any unstable iterations

% initial subspace
if ~start_close
    Q0 = randn(n,m);
    [Q0,~] = qr(Q0,'econ');    
    d = sort(eig(full(A)), 'descend');
    nb_RR_steps = 30;
else
    [V,D] = eig(full(A));
    [d,ind] = sort(diag(D), 'descend');
    V = V(:,ind);

    gap = d(p) - d(p+1)
    sqrt_gap = sqrt(gap)
    N = randn(n,m); N = N/norm(N,'fro');
    V1 = V(:,1:p);
    Q0 = V1 + sqrt(gap)*N*1e-1;
    [Q0,~] = qr(Q0,'econ');
    
    dist_Q0 = subspace(V1, Q0)
    pause
    nb_RR_steps = 30;
end

%%% Compute the optimal Chebyshev filter using the exact eigenvalues
% if with_Cheby
% do this below only once, result is saved in mat file
%   d = sort(eig(full(A)), 'descend');
%   save('evals_fd3d_35_40_1.mat', 'd');
% 

lmin_unwanted = d(end);
lmax_unwanted = d(p+1);

% do iterations 
% `its` stores nb of matvecs with A (or A-cI)

%% Standard SI on B = a(A-cI) which is optimal degree 1 poly for given unwanted spectrum
[W0, T0, trc0, res0, its0] = subsit0(A, Q0, maxit, nb_RR_steps, lmax_unwanted, lmin_unwanted);

%% Standard SI on B = p_d(A) with optimal degree d = nb_RR_steps poly for given unwanted spectrum
[W3, T3, trcCh, resCh, itsCh] = subsitCh(A, Q0, maxit, nb_RR_steps, lmax_unwanted, lmin_unwanted);
[~,  ~, trcCh_b, resCh_b, itsCh_b] = subsitCh(A, Q0, maxit, nb_RR_steps/2, lmax_unwanted, lmin_unwanted);
[~,  ~, trcCh_c, resCh_c, itsCh_c] = subsitCh(A, Q0, maxit, nb_RR_steps*2, lmax_unwanted, lmin_unwanted);

%% Riemanian SD / CG on A (applying the poly filter has no benefit)
opts.TOL_REL_RESIDUAL = 0;
opts.SAVE_EXTRA_MULT_A = true;
opts.EXTRA_ORTH_MOD_ITS = Inf;
opts.RESET_BETA_MOD_ITS = 75;
[W1, T1,trcSD, resSD, itsSD] = blockRQ_RCG(A, Q0, maxit, floor(nb_RR_steps/2), false, opts);
[W2, T2,trcCG, resCG, itsCG] = blockRQ_RCG(A, Q0, maxit, floor(nb_RR_steps/2), true, opts);

%% LOPBCG with no prec 
[WLO, TLO, trcLO, resLO, itsLO] = lobpcg_driver(A, Q0, floor(maxit/2), floor(nb_RR_steps/2), false, opts.TOL_REL_RESIDUAL);


%%
% f_optim = max([sum(d(1:p)), max(trcLO), max(trcCG)]);
f_optim = sum(d(1:p));

cond_H = (d(1)-d(n))/(d(p)-d(p+1));
% % theoretical asymptotic conv factors of SI, RSD, and RCG (not proven)
conv_SD = ((cond_H - 1)/(cond_H +1))^2;
conv_CG = ((sqrt(cond_H) - 1)/(sqrt(cond_H) +1))^2;

nice_cols = plot_defaults();

%% plot the trace
figure(1);
st = 1; 
%% standard SI
semilogy(its0(st:end),f_optim-trc0(st:end), '-','linewidth', 2, 'color', nice_cols(1,:))
hold on
%% Chebyshev SI
semilogy(itsCh_b(st:end),f_optim-trcCh_b(st:end), '-x','linewidth',2, 'color', nice_cols(3,:))
semilogy(itsCh(st:end),f_optim-trcCh(st:end), '-s','linewidth',2, 'color', nice_cols(3,:))
semilogy(itsCh_c(st:end),f_optim-trcCh_c(st:end), '-+','linewidth',2, 'color', nice_cols(3,:))

%% Riemannian SD
semilogy(itsSD(st:end),f_optim-trcSD(st:end), '-','linewidth',2, 'color', nice_cols(2,:));
%% Riemannian CG
semilogy(itsCG(st:end),f_optim-trcCG(st:end), '-o','linewidth',2, 'color', nice_cols(2,:));

%% LOPBCG
semilogy(itsLO(st:end),f_optim-trcLO(st:end), '-o','linewidth',2, 'color', nice_cols(4,:));

xlabel('Number of matvecs with (shifted) $A$')
ylabel('Error in objective value $\phi$')

h1=legend('SI shift', 'SI Cheb 15', 'SI Cheb 30', 'SI Cheb 60', 'Riem SD', 'Riem CG', 'LOBCG');

set(h1,'fontsize',16,'location','SouthEast')
h1.AutoUpdate = 'off'; % do not add lines below



%% plot convergence slopes for RCG

if ~start_close
    axis([0 maxit 1e-13 1e2])
    add_convergence_slope(f_optim-trcSD(st:end), itsSD, conv_SD, 5);
    add_convergence_slope(f_optim-trcCG(st:end), itsCG-30, conv_CG, 2e3);
    saveas(gcf,'figs_paper/e1_fd_subs_Cheby_trace','epsc')
else
    axis([0 400 1e-16 1e2])
    saveas(gcf,'figs_paper/e1_fd_subs_Cheby_trace_close','epsc')
end


