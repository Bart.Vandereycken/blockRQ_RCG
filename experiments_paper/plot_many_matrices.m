function plot_many_matrices()
% Code to reproduce Figs 6.2-5

% First do run_many_matrices() to produce the computation stats.

files = dir('computations/full_*.mat');


for k=1:length(files)
    filename = files(k).name;

    S = load(sprintf('computations/%s',filename));
    prob_name = strrep(extractBefore(filename,".mat"), '_', '-');
    prob_name = strrep(prob_name, "full-", "");
    disp(sprintf('Loading %s', filename))
    disp(sprintf('with prob_name %s', prob_name))

    if sum(S.stats.eigs.flag)
        warning(sprintf('Eigs did not converge for % s!', prob_name))
        methods = {'SD', 'CG'};
    else
        methods = {'SI0', 'SICheb', 'SD', 'CG'};
        legend_names = {'SI', 'SI-Cheb', 'RSD', 'RCG'};
    end
    S.stats


    p = size(S.Q.CG,2);
    Qex = S.stats.eigs.evecs; Qex = Qex(:,1:p);
    dex = S.stats.eigs.evals_eigs; dex = dex(1:p);
    f_ex = sum(dex);
    for m = methods
        meth = m{1};
        Q = S.Q.(meth);
        p = size(Q,2);
        err_angle.(meth) = subspace(Q,Qex);
        d = sort(eig(S.Dq.(meth)), 'descend');
        err_eval.(meth) = norm(d-dex, 'inf')/norm(dex, 'inf');

    end


    err_angle
    err_eval

    nice_cols = plot_defaults();

    close all
    figure(1);
    i = 1;
    for i = 1:length(methods)
        meth = methods{i};
        semilogy(S.its.(meth),S.res.(meth), '-s','linewidth',2, 'color', nice_cols(i,:), 'DisplayName', legend_names{i})
        hold on
        i = i+1;
    end
    title(prob_name)
    legend()
    xlabel('Number of matvecs with (shifted) $A$')
    ylabel('Residual in infty norm')
    saveas(gcf,sprintf('figs_paper/residual_%s',prob_name),'epsc')


    close all
    figure(2);
    i = 1;
    for i = 1:length(methods)
        meth = methods{i};
        semilogy(S.its.(meth),(f_ex - S.trc.(meth))/abs(f_ex), '-s','linewidth',2, 'color', nice_cols(i,:), 'DisplayName', legend_names{i})
        hold on
        i = i+1;
    end
    title(prob_name)
    legend()
    xlabel('Number of matvecs with (shifted) $A$')
    ylabel('Relative error in function value')
    saveas(gcf,sprintf('figs_paper/fval_%s',prob_name),'epsc')

end


return
