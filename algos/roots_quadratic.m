function [x1,x2] = roots_quadratic(coeff)
% Compute the roots x1 <= x2 of c_1 x^2 + c_2 x + c_3 = 0
% in a numerically accurate way

a = coeff(1);
b = coeff(2);
c = coeff(3);

% D = b^2 - 4*a*c;
% x1 = (-b + sqrt(D))/(2*a);
% x2 = (-b - sqrt(D))/(2*a);
m = max([abs(a) abs(b) abs(c)]);
w = b+sign(b)*m*sqrt( (b/m)*(b/m)-4*(a/m)*(c/m) );
x1 = -w/(2*a);
x2 = -(2*c)/w;

if x2<0
    x1b = x1;
    x1 = x2;
    x2 = x1b;
end
