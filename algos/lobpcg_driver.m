function [W, T, trc, res, its, times] = lobpcg_driver(A, Q0, maxit, nb, alwaysExplicitGram, residualToleranceInf)
% nb subsamples outout
%  LOBPCG does 2 block matvec per iteration

[W, T,failureFlag, lambdaHistory, residualNormsHistory, nb_matvecs, times, residualNormsHistoryInf] = lobpcg(Q0, -A, 1e-15, maxit, 0, alwaysExplicitGram, residualToleranceInf);
lambdaHistory = -lambdaHistory;
its = 1:size(lambdaHistory,2);
trc = sum(lambdaHistory);
res = max(residualNormsHistory); % each column has the 2 norm of the residual vectors

% nb_matvecs is per vector, we report per block
% nb_matvecs = nb_matvecs/size(Q0,2);

its = its(1:nb:end);
trc = trc(1:nb:end);
res = residualNormsHistoryInf(1:nb:end);
times = times(1:nb:end);

T = diag(T);

