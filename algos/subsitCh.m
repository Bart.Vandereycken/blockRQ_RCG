function [Q, Dq, trc, res_out, its]= subsitCh(A, Q, nit, nb_RR_steps, lmax_unwanted, lmin_unwanted)
%% function [Q, Dq, trc, res_out, its]= subsitCh(A, Q, nit, p, sigma)
%% A = matrix
%% Q = initial set of vectors on entry - output=basis
%% nit = number of iterations
%% p = number of desired eigenvalues = p dominant ones sought
%% on return
%% Q   = orth. basis of invariant subspace
%% T   = Q'*A*Q = projection of A on Q.
%%------------------------------------------------


% cc = sigma(1);
% hh = sigma(2);
% 
% dd = 2.0 / hh;
[n, m] = size(Q);
[Q, R] = qr(Q,0);
% B = dd*(A - cc*speye(n));

deg = nb_RR_steps;
s1 = 0.5*(lmax_unwanted+lmin_unwanted); s2 = 0.5*(lmax_unwanted-lmin_unwanted); 
B = 1/s2 * (A - s1*speye(n));  % unwanted e-vals are in [-1,1]
B = 2*B; % for easier recursion below


k = 0;

% Initial residual, just for bookkeeping
Dq = Q'*A*Q;
R   = A*Q - Q*Dq;    
res =  norm(R,'inf');
fprintf(1,' it %3d ; res: %8.3e ',k,res);
ritz = sort(eig(Dq), 'descend');
fprintf(1,' Ritz values max,min %10.8f %10.8f\n',ritz(1),ritz(end))
%%==================== total number of matvecs up to this point
k = k+1;
%%--------------------  save trace
trc(1) = trace(Dq) ;
res_out(1) = res;
its(1) = k;

%%--------------------
outer = fix(nit / deg);
for it=1:outer
    %%-------------------- 
    %%3 term recurrence?  -> From Chebyshev Filter 10.1103/PhysRevE.74.066704 
    for jj = 1:deg
        if (jj == 1)
       	 AQ = 0.5*(B*Q)  ;
        else
            %%	 AQ = B*Q;
       	 AQ = B*Q - Q0;
        end
        Q0 = Q;
        Q = AQ;
    end
    [Q, R] = qr(Q,0);
    AQ = A*Q;  % this matvec can be saved so not counted below in k
    Dq = Q'*AQ;    
    %[U,Dq] = eig(Dq);
    %Q = Q*U;
    %AQ  = AQ*U;
    %[~, ind] = sort(diag(Dq),'descend');
    %ind = ind(1:p);
    %%-------------------- shifts cancel out -- no need to adjust
    R   = AQ - Q*Dq;    
    res =  norm(R,'inf');
    fprintf(1,' it %3d ; res: %8.3e ',k,res);
    ritz = sort(eig(Dq), 'descend');
    fprintf(1,' Ritz values max,min %10.8f %10.8f\n',ritz(1),ritz(end))
    %%==================== total number of matvecs up to this point
    k = k+deg;
    %%--------------------  save trace, it+1 since it=1 records start
    trc(it+1) = trace(Dq) ;
    res_out(it+1) = res;
    its(it+1) = k;
end
end


