function [Q,R] = qr_diag_pos(A)
% QR decomposition keeping the diagonal entries non-negative.
% qr (A) = Q*R
% Q*R = Q*P'*P*R 
% set arg(diag(P)) = -arg(diag(R))
% qr_diag_pos (A) = (Q*P') * (P*R)

[Q,R] = qr(A, 'econ');
idx_arg_diag_RR_nonzero = find(diag(R)<0);

Q(:,idx_arg_diag_RR_nonzero) = -Q(:,idx_arg_diag_RR_nonzero);
R(idx_arg_diag_RR_nonzero,:) = -R(idx_arg_diag_RR_nonzero,:);

return

