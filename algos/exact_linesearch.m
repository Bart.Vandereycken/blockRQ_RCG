function [al, stats, Sp, Up, f_val] = exact_linesearch(Q, P, AQ, AP, QAQ, P_is_G, f_old, opts)
% Exact linesearch for block Rayleigh quotient at Q in the direction P
% for the SPD matrix A. Matrices AQ and AP are pre-computed A*Q and A*P.
%
% This linesearch is fairly cheap since AQ and AP are given.
%
% If P_is_G = true, G is the Riemannian gradient, which simplifies
% the linesearch (and makes it more accurate numerically).

if nargin==7 && isfield(opts, 'MONITOR_MONOTONE')
    MONITOR_MONOTONE = opts.MONITOR_MONOTONE;
else
    MONITOR_MONOTONE = false;
end

%%% Hard-coded options - change with care
% Unless all Dc>0, there is no guarantee for a bracket.
% When Dc<0, we can adjust the search direction P to ignore the offending
% directions. This is TRUE by default. Otherwise, we reset P to gradient,
% which wastes work but could be more accurate.
if nargin==7 && isfield(opts, 'NEGATIVE_DC_ADJUST')
    NEGATIVE_DC_ADJUST = opts.NEGATIVE_DC_ADJUST;
else
    NEGATIVE_DC_ADJUST = true;
end

% Finding a zero of phi'(t) is numerically more accurate than an extremum
% of phi(t). It is also easier: the LS routine for phi' only takes 10 steps
% whereas on phi it takes 40.
if nargin==7 && isfield(opts, 'SEARCH_DERIV_LS')
    SEARCH_DERIV_LS = opts.SEARCH_DERIV_LS;
else
    SEARCH_DERIV_LS = true;
end

% Info level
if nargin==7 && isfield(opts, 'INFO_LEVEL')
    INFO_LEVEL = opts.INFO_LEVEL;
else
    INFO_LEVEL = 0;
end

% If some columns in Q have converged, the quadratics become very flat.
% To detect this instabilty, we filter out very large roots.
% A better strategy might be to lock these columns.
if nargin==7 && isfield(opts, 'MAX_UNSTABLE_ROOT')
    MAX_UNSTABLE_ROOT = opts.MAX_UNSTABLE_ROOT;
else
    MAX_UNSTABLE_ROOT = 1e12;
end

THRESHOLD_NEG_CURV = 0;

%%%% Computation: the SVD is more accurate but not need when doing CG
if P_is_G
    [~,S,U] = svd(P, 'econ');
    Sp = S*S; Up = U;
    Db = diag(S).^2;  % beta in paper
else
    [U,DD] = eig(P'*P);
    Db = diag(DD);
    Sp = DD;
    Up = U;
end

% this can be done faster in principle, since we only need the diagonals...
if P_is_G
    Dc = Db;
else
    Dc = diag(U'*(Q'*AP)*U);  % chi in paper
end
Da = U'*(QAQ)*U; 
Da = diag(Da);  % alpha in paper 
Dg = U'*(P'*AP)*U; 
Dg = diag(Dg);  % gamma in paper


stats = [];
ind_neg = [];

if min(Dc)<THRESHOLD_NEG_CURV  % should never happen if P_is_G
    if INFO_LEVEL>0
        disp('Dc has negative values which does not allow us to guarantee a bracket as is...');    
        Dc(Dc<0)'
    end
    % If Dc has neg values, we are not certain of the bracketing interval.
    % Most the time the LS will still work but close to the solution, more
    % and more negative values pop up. We can reset to gradient since that
    % always gives Dc>0.
    
    if NEGATIVE_DC_ADJUST
        if INFO_LEVEL>1
            disp('... removing columns that correspond to negative Dc');    
        end        
        ind_neg = find(Dc<THRESHOLD_NEG_CURV);
    else
        if INFO_LEVEL>0
            disp('... resetting to gradient');    
        end
        al = 0;
        f_val = f_old;
        return
    end
end


%%% Compute the roots of the quadratics in the numerator to determine
%   search bracket.
coeff = [Db.*Dc Da.*Db-Dg -Dc];
roots_numer = [];
for i = 1:size(coeff,1)
    [x1,x2] = roots_quadratic(coeff(i,:));
    if x1>0
        [x1, x2]
        warning('The smallest root should always be negative or zero.')
    end
    if ismember(i,ind_neg)
        if INFO_LEVEL>1
            disp('skipping direction')
        end
    else
        roots_numer(i) = x2;        
    end
end    

if size(roots_numer,1)==0
    if INFO_LEVEL>0
        disp('No bracket found. Restart with gradient!!');
    end
    al = 0;
    f_val = f_old;
    return
end


if max(roots_numer) > MAX_UNSTABLE_ROOT
    if INFO_LEVEL>0
        disp('Very large root found. Ignoring...')
    end
    roots_numer = roots_numer(roots_numer<MAX_UNSTABLE_ROOT);
end
mub = max(roots_numer);
mua = 0; % always works, if not, we have probably converged
%mua = min(roots_numer); % decreases the LS steps just a tiny bit

dfa = eval_deriv_LS(Da, Db, Dc, Dg, mua);
dfb = eval_deriv_LS(Da, Db, Dc, Dg, mub);

if dfa<0
    if INFO_LEVEL>0
        disp('Not descent. Restarting with gradient!!') 
    end
    al = 0; % Signal for outer loop
    f_val = f_old;
    return
end
if isempty(dfa) || isempty(dfb)
    if INFO_LEVEL>0
        disp('Empty bracket for LS. Restarting with gradient!!') 
    end
    al = 0; % Signal for outer loop
    f_val = f_old;
    return
end

% For safety, we verify that the f'(a) and f'(b) have different slopes.
nb_back = 0;
while dfa*dfb>0
    mub = mub/2;
    dfb = eval_deriv_LS(Da, Db, Dc, Dg, mub);
    if INFO_LEVEL>0
        disp('No valid bracket found. We back track the right point for 15 steps.')
    end
    nb_back = nb_back + 1;

    if nb_back>20
        if INFO_LEVEL>0
            disp('Backtracking exhausted. Restarting with gradient!!') 
        end
        al = 0; % Signal for outer loop
        f_val = f_old;
        return
    end
end

% Line search on the given bracket with full numerical precision
% mua = 0;
opts.TolFun = eps;
opts.TolX = eps;
opts.Display = 'off';
if SEARCH_DERIV_LS
    [al,fzeroval,~,output] = fzero(@(mu)(eval_deriv_LS(Da, Db, Dc, Dg, mu)),[mua,mub], opts);
    f_val = eval_LS(Da, Db, Dc, Dg, al);    

else
    [al,fval,~,output] = fminbnd(@(mu)(-eval_LS(Da, Db, Dc, Dg, mu)),mua,mub, opts);
    f_val = - f_val;
end
stats.LS_evals = output.funcCount;

% seems to be very small, about 10 for SEARCH_DERIV_LS=true
if(output.funcCount>200)
    warning('More than 200 pts evaluated during LS. This could be a sign of numerical trouble.')
end





if MONITOR_MONOTONE && f_val<f_old
    warning('New step of linesearch was not monotonic. Resetting to gradient.')%Backtracking..
    al = 0; % Signal for outer loop
    f_val = f_old;
    return
end


end

%%% The objective value along the curve mu: phi(mu) -- vectorized
function mus = eval_LS(Da, Db, Dc, Dg, mu)
npts = length(mu);
fn = Da .* ones(1,npts) + 2*Dc .* mu + Dg .* (mu .^2);
fd = 1 + Db .*  (mu .^2);
mus = sum(fn./fd, 1);
end

%%% The derivative of phi wrt mu  -- vectorized.
function mus = eval_deriv_LS(Da, Db, Dc, Dg, mu)
npts = length(mu);
fn = Dc .* ones(1,npts) + (Dg - Da .* Db) .* mu - (Db .* Dc) .* (mu .^2);
fd = (1 + Db .*  (mu .^2)) .^2;
mus = sum(fn./fd, 1);
end


