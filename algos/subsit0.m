function [Q, Dq, trc, res_out, its]= subsit0(A, Q, nit, nb_RR_steps, lmax_unwanted, lmin_unwanted)
%% function [Q, Dq, trc, res_out, its]= subsit0(A, Q, nit, p, sigma)
%% A = matrix
%% Q = initial set of vectors on entry - output=basis
%% nit = number of iterations
%% p = number of desired eigenvalues = p dominant ones sought
%% sigma(1) = middle of interval to damp in Chebyshev
%% sigma(2) = half width of same interval
%% sigma(3) = (integer) = period for doing Rayleigh-Ritz
%% on return
%% Q   = orth. basis of invariant subspace
%% Dq  = Q'*A*Q = projection of A on Q.
%% rest is for plotting
%%------------------------------------------------
[n, p] = size(Q);
[Q, ~] = qr(Q,0);
k = 0;
%%--------------------
s1 = 0.5*(lmax_unwanted+lmin_unwanted); s2 = 0.5*(lmax_unwanted-lmin_unwanted); 
s2
s1
B = 1/s2 * (A - s1*speye(n));  % unwanted e-vals are in [-1,1]

nmv = 0;

% Initial residual, just for bookkeeping
Dq = Q'*A*Q;
R   = A*Q - Q*Dq;    
res =  norm(R,'inf');
fprintf(1,' it %3d ; res: %8.3e ',nmv, res);
ritz = sort(eig(Dq), 'descend');
fprintf(1,' Ritz values max,min %10.8f %10.8f\n',ritz(1),ritz(end))
%%==================== total number of matvecs up to this point
nmv = nmv+1;
%%--------------------  save trace
trc(1) = trace(Dq) ;
res_out(1) = res;
its(1) = nmv;

% cc = sigma(1);
% hh = sigma(2);
deg = nb_RR_steps;
% dd = 2.0 / hh;
% B = 2*dd*(A-cc*speye(n)); % unwanted e-vales are now in [-1,1]
% B = A-cc*speye(n);  % Saad's book section 5.3.2
%%-------------------- counts number of matvecs

for it=1:nit
    %%-------------------- in all cases:
    AQ = B*Q;
    Q = AQ;
    nmv = nmv+1;
    %%-------------------- step + projection
    if (mod(it,deg) == 0 || it==nit)
        [Q, R] = qr(AQ,0);
        AQ = A*Q;
        nmv = nmv+1;
        k = k+1;
        Dq = Q'*AQ ;
        %      Dq = (Dq/dd + cc*eye(m));
        [U,Dq] = eig(Dq);
        Q = Q*U;
        AQ = AQ*U;
        [~, ind] = sort(diag(Dq),'descend');
        ind = ind(1:p);
        %%-------------------- shifts cancel out -- no need to adjust        
        R = AQ(:,ind) - Q(:,ind)*Dq(ind,ind);
        %res =  norm(R,'fro');
        res =  norm(R,'inf');
        fprintf(1,' it %3d ; res: %8.3e ',it,res);
        ritz = sort(eig(Dq), 'descend');
        fprintf(1,' Ritz values max,min %10.8f %10.8f\n',ritz(1),ritz(end))
        %%--------------------  save trace
        trc(k+1) = trace(Dq) ;
        res_out(k+1) = res;
        its(k+1) = nmv;
    end
    %%-------------------- step only
    %%end
end


