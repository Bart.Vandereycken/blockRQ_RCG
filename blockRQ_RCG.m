function [Q, Dq, trc, res_out, its, times, restarts, stats] = blockRQ_RCG(A, Q, nit, it_out, do_CG, opts)
%BLOCKRQ_RCG Riemannian CG for block Rayleigh quotient.
%
% Implementation of [1] for minimizing the block Rayleigh quotient
%     tr(Q' A Q) st Q' Q = I
% of a symmetric matrix n-by-n A over the orthonormal n-by-k matrices Q
% using Riemannian CG with an exact but efficient linesearch.
%
% The method performs one block matvec with A and Q at every iteration.
% Thanks to the exact linesearch it is parameter free. 
%
% Inputs: A      -- symmetric matrix A
%         Q      -- initial value Q_0
%         nit    -- number of iterations
%         it_out -- compute certain output (see below) at iterations
%                   I = (it_out,  2 it_out, ... )
%         do_CG  -- perform nonlinear CG if true (default), otherwise SD
%         opts   -- more fine tuned options (see code below)
%
% Return: Q        -- optimized Q_i at iteration i \in I
%         Dq       -- Q' A Q with optimized Q_i at i \in I
%         trc      -- tr(Q_i) at i \in I
%         res_out  -- inf-norm of residual at i \in I
%         its      -- set I above
%         restarts -- nb of restarts to SD due to failing linesearch
%         times    -- time of tic-toc at i \in I
%
% [1] F. Alimisis, Y. Saad, B. Vandereycken, Gradient-type subspace 
%     iteration methods for the symmetric eigenvalue problem, 2024.
%     arXiv:2306.10379 (<https://arxiv.org/pdf/2306.10379.pdf>)
%     Published in SIAM Journal on Matrix Analysis and Applications

if nargin==5
    opts = [];
end

% We can save an extra multiplication by A, but there is very small loss of
% accuracy. Enabled by default. Can also give unstable iteration if one
% keeps iterating when the method has converged.
if nargin==6 && isfield(opts, 'SAVE_EXTRA_MULT_A')
    SAVE_EXTRA_MULT_A = opts.SAVE_EXTRA_MULT_A;
else
    SAVE_EXTRA_MULT_A = true;
end

% The algorithm is in theory monotonic in function value. Due to numerical
% cancellation, this good property is violated sometimes. We can monitor
% this and restart the method if that happens. It requires a little more
% memory to keep the old iteration.
if nargin==6 && isfield(opts, 'MONITOR_MONOTONE')
    MONITOR_MONOTONE = opts.MONITOR_MONOTONE;
else
    MONITOR_MONOTONE = false;
end

% Do an explicit QR orthognalization every
% EXTRA_ORTH_MOD_ITS steps.
if nargin==6 && isfield(opts, 'EXTRA_ORTH_MOD_ITS')
    EXTRA_ORTH_MOD_ITS = opts.EXTRA_ORTH_MOD_ITS;
else
    EXTRA_ORTH_MOD_ITS = Inf; 
end

if nargin==6 && isfield(opts, 'RESET_BETA_MOD_ITS')
    RESET_BETA_MOD_ITS = opts.RESET_BETA_MOD_ITS;
else
    RESET_BETA_MOD_ITS = Inf; 
end

% Stopping tolerance on residual reduction compared to staring residual.
if nargin==6 && isfield(opts, 'TOL_REL_RESIDUAL')
    TOL_REL_RESIDUAL = opts.TOL_REL_RESIDUAL;
else
    TOL_REL_RESIDUAL = 1e-14; 
end

t_begin = tic();
restarts = 0;% nb of restarts to RSD
nb_matvecs = 0; % record nb of matvecs like A*Q
out_i = 1; % counter for the vectors of outputs

%%% Start iteration
[Q, ~] = qr(Q,0);
AQ = A*Q; nb_matvecs = nb_matvecs + 1;

Dq = Q'*AQ;
G = AQ - Q*Dq;
P = G;
P = P - Q*(Q'*P); 
den = trace(P'*P);

%%% Record residual at start
res =  norm(G,'inf'); res_init = res;
fprintf(1,' it %3d ; res: %8.3e ',nb_matvecs,res);
fprintf(1,' T11,T22 %10.8f %10.8f\n',Dq(1,1),Dq(2,2))

trc(out_i) = trace(Dq);
res_out(out_i) = res;
its(out_i) = nb_matvecs;
times(out_i) = toc(t_begin);
out_i = out_i + 1;

f_old = -inf; % we maximize RQ

for it=1:nit-1
    restarted = false;
    AP = A*P; nb_matvecs = nb_matvecs + 1;

    [mu, ~, Sp, Up, f_val] = exact_linesearch(Q, P, AQ, AP, Dq, ~do_CG, f_old, opts); % signal if P is G or not    

    if mu==0 % signal that we need to restart with RSD
        %fprintf('Restart CG direction to gradient direction due to bad LS \n')
        restarted = true; restarts = restarts+1;
        P = G;
        AP = A*P; nb_matvecs = nb_matvecs + 1;

        P = P - Q*(Q'*P); 
        den = trace(P'*P);

        [mu, ~, Sp, Up, f_val] = exact_linesearch(Q, P, AQ, AP, Dq, true, f_old, opts); % signal that we always have gradient as P
        if mu==0
            warning('No step found')
            return
        end

    end
    %%% Do the step
    if MONITOR_MONOTONE
        Q_old = Q; AQ_old = AQ; Dq_old = Dq;
    end

      
    QQ = Q+mu*P;    
    if mod(it,EXTRA_ORTH_MOD_ITS)==0
        [Q,R] = qr(QQ,'econ');
        % [Q,R] = qr_diag_pos(QQ);
        with_R_inv = false;
    else

    sp = diag(Sp);                   
    S_inv = diag(1./sqrt(1+mu^2 *sp));
    Q = ((QQ*Up)*S_inv)*Up';
    with_R_inv = true;
    end
        
    % We can compute the new AQ = A*Q but avoid an extra mult. with A
    % This has a small impact on accuracy (1e-14 instead of 1e-15 at
    % convergence). If we restarted above, it is safer to recompute AQ
    % since such restarts are usually due to roundoff errors.   
    if SAVE_EXTRA_MULT_A && ~restarted
        % If mu*AP is very small (<eps) compared to AQ, we should not do this
        % update since we do not bring in any new information 
        % Above we have sing values of P in Sp.
        % Do a test with Q vs mu*P
        if mu*max(abs(Sp))<4*eps
            %disp('Skipping updated for SAVE_EXTRA_MULT_A')
            [Q,~] = qr_diag_pos(Q);
            AQ = A*Q; %nb_matvecs = nb_matvecs + 1;
        else
            if with_R_inv
                AQ = (((AQ + mu*AP)*Up)*S_inv)*Up';
            else
                AQ = (AQ + mu*AP)/R;
            end
        end
    else
        AQ = A*Q; %nb_matvecs = nb_matvecs + 1;
    end
    Dq = Q'*AQ;
    f_new = trace(Dq);
    f_delta = f_new-f_old;
       

    if MONITOR_MONOTONE && f_delta<0 % we maximize
        fprintf('Not monotonic at outer loop it %i; f delta is %8.3e! \n', it, f_delta)
        disp('Discarding this step and resetting to gradient.')

        Q = Q_old; AQ = AQ_old; Dq = Dq_old; P = G;
    else

        f_old = f_new;

        
        if abs(f_val-f_new)>1e-13*abs(f_val)
            %warning('Numerical issue: update f is not the same as it should be')
            fprintf('Difference in f_val %8.3e! \n', abs(f_val-f_new)/abs(f_val))
        end

        %%% Polak-Ribiere
        if do_CG            
            G0 = G;
        end

        G = AQ - Q*Dq;

        if ~do_CG
            P = G;

        else        
            den1 = G(:)'*G(:);            
            t = G(:)'*G0(:);            
            
            

            if mod(it,RESET_BETA_MOD_ITS)==0
                beta = 0;
            else
                beta = (den1-t) / den;
            end
            den = den1;

            P = G + beta*P;                      
        end    


        QP = Q'*P;
        P = P - Q*QP;
    end


    %%% Record residual every it_out steps
    if mod(it,it_out)==0
        res =  norm(G,'inf');
        fprintf(1,' it %3d ; res: %8.3e ; f delta: %8.3e ',nb_matvecs,res, f_delta);
        ritz = sort(eig(Dq), 'descend');
        fprintf(1,' Ritz values max,min %10.8f %10.8f\n',ritz(1),ritz(end))

        trc(out_i) = trace(Dq);
        res_out(out_i) = res;
        its(out_i) = nb_matvecs;
        times(out_i) = toc(t_begin);
        out_i = out_i + 1;

        if res < TOL_REL_RESIDUAL * res_init
            disp("We reached the tolerance on the relative residual. Stopping.")
            break
        end
    end

end
%%
